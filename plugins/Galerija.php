<?php

/**
 * Galerija - Responsive gallery in shortcode format
 * 
 * @author  Jurij Podgoršek <g1smo@git.kompot.si>
 * @license https://www.gnu.org/licenses/agpl-3.0.en.html
 * @version 0.1
 */
class Galerija extends AbstractPicoPlugin
{
    /**
     * API version used by this plugin
     *
     * @var int
     */
    const API_VERSION = 3;

    /**
     * This plugin depends on ...
     *
     * @see AbstractPicoPlugin::$dependsOn
     * @var string[]
     */
    protected $dependsOn = array();

    protected $useImagick = false;

    const SHORTCODE = '#\[galerija .+?\]#i';

    /**
     * Triggered after Pico has read its configuration
     *
     * @see Pico::getConfig()
     * @see Pico::getBaseUrl()
     * @see Pico::getBaseThemeUrl()
     * @see Pico::isUrlRewritingEnabled()
     *
     * @param array &$config array of config variables
     *
     * @return void
     */
    public function onConfigLoaded(array &$config)
    {
        $this->folder = '.resized';
        if (isset($config['ImageResize']['folder']))
            $this->folder = $config['ImageResize']['folder'];

        $this->quality = 85;
        if (isset($config['ImageResize']['quality']))
            $this->quality = $config['ImageResize']['quality'];

        $this->width = 500;
        if (isset($config['ImageResize']['width']))
            $this->quality = $config['ImageResize']['width'];

        $this->height = 300;
        if (isset($config['ImageResize']['height']))
            $this->quality = $config['ImageResize']['height'];
    }

    /**
     * Triggered when Pico registers the twig template engine
     *
     * @see Pico::getTwig()
     *
     * @param Twig_Environment &$twig Twig instance
     *
     * @return void
     */
    public function onTwigRegistered(Twig_Environment &$twig)
    {
        if (extension_loaded('imagick'))
            $this->useImagick = true;
        elseif (!extension_loaded('gd'))
            exit('PHP extension "imagick" or "gd" is not installed, or not enabled in php.ini');

        $twig->addFunction(new Twig_SimpleFunction('resize', array($this, 'resize')));
    }

    /**
	 * Triggered after Pico has prepared the raw file contents for parsing
	 *
	 * @see	Pico::parseFileContent()
	 * @see	DummyPlugin::onContentParsed()
	 * @param  string &$content prepared file contents for parsing
	 * @return void
	 */
    public function onContentParsed(&$content)
    {
        if (stripos($content, '[galerija') !== false) {
            // Search for Embed shortcodes allover the content
            preg_match_all(self::SHORTCODE, $content, $matches);

            // Make sure we found some shortcodes
            if (count($matches[0]) > 0) {

                // Walk through shortcodes one by one
                foreach ($matches[0] as $match) {
                    $p = explode(' ', $match)[1];
                    $pot = substr($p, 0, strlen($p) - 1);
                    $content = preg_replace(self::SHORTCODE, $this->getGalerija($pot), $content, 1);
                }
            }
        }
    }

    protected function getGalerija($path) {
        $slike = glob($path . DIRECTORY_SEPARATOR . '*');
        asort($slike);

        $galerija = '<section class="galerija">' . "\n";
        foreach ($slike as $s) {
            if (is_file($s)) {
                //var_dump($this->twig);die;
                $thumb = $this->resize($s, $this->width, $this->height);
                $lightbox = "data-lightbox=\"hue\"";
                //$galerija .= "<img alt=\"$s\" src=\"$thumb\" $lightbox>\n";
                $galerija .= "<a href=\"$s\" $lightbox><img alt=\"$s\" src=\"$thumb\"></a>\n";
            }
        }
        $galerija .= '</section>' . "\n";

        return $galerija;
    }

    /*
protected function onContentLoading($file) {
        var_dump('loading', $file);
    }
    */
    /**
     * Resize an image, save it to a temporary folder and return new filename
     * @param string $file
     * @param int $width
     * @param int $height
     * @return string
     */
    public function resize($file, $width = null, $height = null)
    {
        if (is_null($width) && is_null($height)) {
            error_log(new InvalidArgumentException("Width and height can't both be null"));
            return $file;
        }

        // determine resized filename
        $newFile = sprintf('%s/%s/%s-%dx%d.jpg',
            dirname($file),
            $this->folder,
            pathinfo($file, PATHINFO_FILENAME),
            $width,
            $height
        );

        // if we have already resized, just return the existing file
        if (file_exists($newFile))
            return $newFile;

        // load file dimensions
        $dimensions = getimagesize($file);
        $originalWidth = $dimensions[0];
        $originalHeight = $dimensions[1];

        // calculate the final width and height (keep ratio)
        $widthRatio = $originalWidth / ($width ?: 1);
        $heightRatio = $originalHeight / ($height ?: 1);
        if ($widthRatio < 1 || $heightRatio < 1) {
            $resizedWidth = $originalWidth;
            $resizedHeight = $originalHeight;
        } else if ($widthRatio < $heightRatio) {
            $resizedWidth = $width;
            $resizedHeight = round($originalHeight / $widthRatio);
        } else {
            $resizedWidth = round($originalWidth / $heightRatio);
            $resizedHeight = $height;
        }

        // make sure folder exists
        if (!file_exists(pathinfo($newFile, PATHINFO_DIRNAME)))
            mkdir(pathinfo($newFile, PATHINFO_DIRNAME));

        // resize and save
        if ($this->useImagick) {
            $image = new Imagick($file);
            $image->setImageCompressionQuality($this->quality);
            $image->thumbnailImage($resizedWidth, $resizedHeight);
            $image->writeImage($newFile);
        } else {
            $image = imagecreatefromstring(file_get_contents($file));
            $newResource = imagescale($image, $resizedWidth, $resizedHeight);
            imagejpeg($newResource, $newFile, $this->quality);
        }

        return $newFile;
    }
}
