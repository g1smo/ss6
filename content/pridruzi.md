---
Title: Pridruži se nam
Order: 3
---
# O nas

Smo predstavniki širše skupnosti prebivalcev Šišenske soeske 6, ki se s svojim delovanjem zavzemamo za zaščito javnih in odprtih zelenih površin. Zavedamo se, da podobno usodo delijo tudi številne druge soseske po Ljubljani in Sloveniji. Naša prizadevanja so absolutno neideološka in nepolitična. Naši člani so predstavniki vseh starostnih skupin in širokega spektra političnih prepričanj.

# Bodi obveščen_a

Lahko se naročiš na obvestilnik preko [te povezave](https://liste.kompot.si/postorius/lists/ss6.kompot.si/).

<!--form action="https://liste.kompot.si/postorius/lists/ss6.kompot.si/subscribe" method="post" id="narocilo">
<label for="id_email">E-poštni naslov:</label>
<input type="text" id="id_email" name="id_email">
<button type="submit" id="obvestilnik_narocilo">Naroči</button>
</form-->

# Pridruži se našim prizadevanjem

## Bodimo slišani

Svoje nasprotovanje načrtom spremembe OPN posreduj mestnim svetnikom, kontaktne podatke najdeš na tej [povezavi](https://docs.google.com/document/d/1x-69AccNGbaQQU9MykRa5J_2i4jbwCqYrucx0CUc0NU/edit?usp=sharing). Za oporo lahko uporabiš šablono, ki smo jo pripravili in je dostopna [tukaj](https://docs.google.com/document/d/1DKI8idnhQXBpLSdjBBZ60y8jx79bq3YmmcSJuB3fbc4/edit?usp=sharing).

## Pomagaj na terenu

Na naslov [sisenskasoseska6_AFNA_gmail.com](sisenskasoseska6_AFNA_gmail.com){.mlf} se javi, če si pripravljen_a sodelovati pri deljenju letakov in zbiranju podpisov.

## Podpri nas z donacijo

Trenutno preučujemo pravne možnosti, ki bi nam omogočile ustaviti načrtovane spremembe.

Vsak evro, ki ga prejmemo iz donacij, bo namenjen za plačilo pravne pomoči.

Za donacijo nas kontaktiraj na [sisenskasoseska6_AFNA_gmail.com](sisenskasoseska6_AFNA_gmail.com){.mlf}

## Ustvarjamo kolektivni spomin
V imenu skupnosti ŠS6 vas prosimo, da nam pomagate ustvariti kolektivni spomin. Pošljite nam vaše slike in spomine. Prav tako vas prosimo, da nam posredujete svoje pretekle izkušnje iz pravdnih sporov in upravnih sporov. Vse našteto lahko anonimno naložite [tukaj](https://obzorje.kompot.si/s/87HkkrQMakoWe5d). Brez vašega privoljenja ne bomo objavili ničesar.

