---
Title: Zgodovina soseske
Order: 0
---

Šišenska soseska 6 (ŠS-6) predstavlja enega kvalitetnejših primerov stanovanjske gradnje v Ljubljani, ki je bila realizirana kot prva zaključena urbanistična enota za 10.000 prebivalcev. Načrtovana je bila skladno z urbanističnimi načeli Ravnikarjeve “idealne soseske” in bila zgrajena v letih 1964-1972.

Naselje leži ob severni mestni vpadnici Celovški cesti. Skozi sosesko poteka sprehajalna pot Pot spominov in tovarištva ali Pot (PST) ob žici (POT), speljana po trasi bodeče žice, s katero je bila Ljubljana ograjena med 2. svetovno vojno. POT sosesko povezuje z zelenim zaledjem griča Rožnik.

Urbanista Aleš Šarec in Janez Vovk sta predvidela pahljačasto zasnovo soseske, ki jo opredeljujejo višinsko hierarhično strukturirana zazidava, mreža peš poti in urbanistični elementi tradicionalnega mesta. Že v času nastanka je soseska v urbanističnem smislu predstavljala velik odklon od funkcionalističnih načel. Visoke, osem do štirinajst nadstropne stanovanjske stolpnice tvorijo središče soseske, ki je locirano ob mestni vpadnici. Z oddaljenostjo od glavne ceste višinski gabariti objektov padajo vse do individualne zazidave.

Stanovanjski bloki z zamaknjenimi osnovnimi stavbnimi enotami tvorijo členjene nize, prostore med njimi pa zasedajo obsežne, z visokodebelnim drevjem zasajene parkovne površine z otroškimi igrišči in vrtovi, ki jih ozelenjeni nasipi delijo na intimnejše odseke. Odprte zelene površine in parki soseske ŠS-6 spadajo med lepše primere krajinskega oblikovanja javnih površin v Ljubljani. V zelenju sredi soseske so šola, vrtec in telovadnica.


VIR: [Kubusarhitektura](https://kubusarhitektura.com/sl/sisenska-soseska-ss-6/)
