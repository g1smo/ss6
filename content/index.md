## Šišenski soseski 6 grozi pozidava!

Predlog OPN, ki bo obravnavan na januarski seji Mestnega sveta, spreminja namembnost zemljišč znotraj ŠS-6, kar bo omogočilo pozidavo zelenih površin, ki trenutno opravljajo pomembno socialno, rekreativno in ekološko funkcijo.

Načrtovani grobi posegi so problematični tudi v urbanističnem smislu, ker bi te spremembe posegle v smiselno zaključeno zasnovo soseske in enega najkakovostnejših primerov oblikovanja mestne krajine pri nas. Prav tako to ne bi bilo v skladu s strateškim ciljem ohranjanje kakovosti bivanja in urbanističnim kriterijem varovanja odprtih zelenih površin v zgoščenih stanovanjskih soseskah.


![ŠS6 slika](%assets_url%/slike/2021-09-07-v-mestu-se-gradi-129290.jpg)



V preteklosti so podobne pobude Video arta d.o.o. na magistratu zavrnili. Občina je takrat pojasnila, da »pobuda ni skladna z urbanističnim kriterijem ohranjanja bivalne kakovosti v obstoječih poselitvenih območjih – soseskah. Obravnavano območje predstavlja zeleno površino strnjenega naselja. Te površine imajo pomembno socialno in ekološko funkcijo, ker omogočajo ureditev prostorov druženja in izboljšujejo mikroklimatske pogoje. Zgoščevanje soseske z gradnjo novih objektov na odprtih zelenih površinah ni sprejemljivo.«

Trenutni argument MOL je, da bi s potencialno gradnjo stanovanj na omenjenih površinah rešili dolgoletni spor z zasebnim investitorjem – torej gre za nekakšno »poravnavo« - na račun tretjega, to je stanovalcev v soseski. Urejanje prostorsko-urbanističnih nepravilnosti se ne bi smelo reševati s »poravnavo«.

Stanovalci ŠS-6 takšni poravnavi odločno nasprotujemo. V preteklih mesecih smo se prešteli z informativnim oz. posvetovalnim zbiranjem podpisov; v le dveh dneh smo jih zbrali več kot 600. Prav tako se zavedamo, da bo predlagana »rešitev« ustvarila precedens, ki bo vzpodbudil tudi ostale lastnike špekulativno pridobljenih zemljišč, da začnejo s pozidavo zelenih površin v gosto naseljenih in urbanistično zaključenih soseskah.

Naši 4 gradniki so: _ljubezen_, **znanje**, _stroka_ in **smeh**.
