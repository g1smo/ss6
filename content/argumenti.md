---
Title: Argumenti proti pozidavi
Order: 2
---

## 1. Bivša stališča MOL:
·        Pobuda ni skladna z urbanističnim kriterijem ohranjanja bivalne kakovosti v obstoječih soseskah.

·        Območje predstavlja zeleno površino strnjenega naselja. Te površine imajo pomembno socialno in ekološko funkcijo, omogočajo ureditev prostorov druženja in izboljšujejo mikroklimatske pogoje.

·       Zgoščevanje soseske z gradnjo novih objektov na odprtih zelenih površinah ni le nesprejemljivo, ampak nevzdržno.

.     Povprečje gostote poseljenosti na km2 je po zadnjih podatkih za Ljubljano iz leta 2019 1.065,4 občanov/km2. Znotraj Šišenske soseske 6 je že sedaj skoraj štiri krat višja gostota prebivalstva, in sicer le-ta znaša več kot  4.100 prebivalcev/km2. (vir: MOL) 
 
## 2. Argumenti stroke:
·       Soseska ŠS-6 predstavlja enega kvalitetnejših primerov stanovanjske gradnje v Ljubljani, ki je bila realizirana kot prva zaključena urbanistična enota.

·        Načrtovana je bila skladno z urbanističnimi načeli Ravnikarjeve “idealne soseske”.

·       Odprte zelene površine in parki soseske ŠS-6 spadajo med lepše primere krajinskega oblikovanja javnih površin v Ljubljani.

·        Zelene površine znotraj sosesk izboljšujejo mikroklimatske pogoje.

·        Podpora Fakultete za arhitekturo (Uni. Lj.).
 
## 3. Stališča stanovalcev:
·       Z gradnjo stanovanj bi rešili dolgoletni spor MOL z zasebnim investitorjem; gre za poravnavo na račun stanovalcev v soseski.

·        Zgoščevanje soseske z gradnjo novih objektov na odprtih zelenih površinah ni sprejemljivo.

·        Pobuda ni skladna z urbanističnim kriterijem ohranjanja bivalne kakovosti v obstoječih soseskah.

·        ŠS-6 ima že v obstoječem stanju nadpovprečno visoko gostoto poseljenosti in je v primerjavi s celotno Ljubljano skoraj štirikrat gosteje poseljena. 

·      Večja gostota poselitve bo negativno vplivala na obremenitev vzporedne infrastrukture (vrtci, šole), ki je že v trenutni situaciji preobremenjena, pričakujemo pa lahko še poslabšanje po zaključku gradenj na drugi strani Celovške (Rastoderjevi stolpnici, Kvartet, …).

·        Ustvarjen precedens bi vodil v nadaljnje spremembe OPN in posledične pozidave, ki bi bile v nasprotju z interesi občanov in etažnih lastnikov.

