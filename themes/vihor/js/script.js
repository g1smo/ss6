window.addEventListener('DOMContentLoaded', function () {
  document.querySelectorAll('.mlf').forEach(function (el) {
    var m = el.getAttribute('href').split(/_AFNA_/);
    var mail = m[0] + '@' + m[1];
    el.setAttribute('href', 'mailto:' + mail);
    el.innerHTML = mail;
  });

  /*
  var narocilo = document.getElementById('narocilo');
  if (narocilo) {
    var e = document.getElementById('id_email');
    var mail = e.value;

    var subUrl = 'https://liste.kompot.si/postorius/lists/ss6.kompot.si/';
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
      if (xhr.readyState == 4 && xhr.status == 200) {
        var m = xhr.response.match(/<input type='hidden' name='csrfmiddlewaretoken'\s+value='([a-zA-Z0-9]+)'/);
        console.log('matching', m);
        if (m.length == 2) {
          var csrf = document.createElement('input');
          csrf.setAttribute('type', 'hidden');
          csrf.setAttribute('name', 'csrfmiddlewaretoken');
          csrf.value = m[1];
          console.log(csrf);
          narocilo.appendChild(csrf);
        }
      }
    }
    xhr.open('GET', subUrl, true);
    xhr.send();
  }
  */
});
